// slice
package main

import (
	"fmt"
)

func f(s []int) []int {
	s = append(s, 3)
	return s
}

func main() {
	s := []int{}
	//	s := make([]int, 0)
	fmt.Println("slice cap =", cap(s))
	fmt.Println(s)
	f(s) //超过cap值后，append的值放到了新拷贝的slice中，新slice的引用丢失
	fmt.Println(s)
	s = f(s) //超过cap值后，append的值放到了新拷贝的slice中，s重新指向新slice
	fmt.Println(s)
}
