/*
	time.Format
	!!!!一定要使用格式标准库中提供的常量或者字符串常量，否则会有意想不到的错误
*/
package main

import (
	"fmt"
	"time"
)

func main() {
	t := time.Now()
	fmt.Println(t)
	fmt.Println(t.Format(time.ANSIC))
	fmt.Println(t.Format("Mon Jan _2 15:04:05 2006"))
	fmt.Println(t.Format("Mon Jan _2 15:04:06 2006"))

}
