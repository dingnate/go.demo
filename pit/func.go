// func
package main

import (
	"fmt"
	"sync"
)

func main() {
	//gorouting
	s := []string{"a", "b", "c"}
	swg := sync.WaitGroup{}
	swg.Add(len(s))
	for _, v := range s {
		go func() {
			fmt.Println(v) //v是for中v的引用，所以会实时的打印for中v的值
			swg.Done()
		}()
	}
	swg.Wait()

	//fix
	fmt.Println("-------------fix-------------")
	for _, v := range s {
		go func(v string) {
			fmt.Println(v) //v是函数参数，在调用函数的时候就获得了for中v的拷贝
		}(v)
	}

	select {} //阻塞main函数
}
