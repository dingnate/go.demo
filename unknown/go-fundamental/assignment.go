package main

import "fmt"

func main() {
	var a, b, c, d int = 1, 2, 3, 4
	fmt.Println(a, b, c, d)
	var e, f, g, h = 1, 2, 3, 4
	fmt.Println(e, f, g, h)
	i, j, k, l := 1, 2, 3, 4
	fmt.Println(i, j, k, l)
	m, _, o, p := 1, 2, 3, 4
	fmt.Println(m, o, p)
}
