package main

import "fmt"

type Usb interface {
	Name() string
	Connector
}
type Connector interface {
	Connect()
}

type PhoneConnector struct {
	name string
}

func (pc PhoneConnector) Name() string {
	return pc.name
}

func (pc PhoneConnector) Connect() {
	fmt.Println("Connect", pc.Name())
}

type TVConnector struct {
	name string
}

func (tv TVConnector) Connect() {
	fmt.Println("Connect", tv.name)

}
func Disconnect(usb interface{}) {
	//通过.(type)类型转换，返回转换后的类型,异常
	//if _, ok := usb.(PhoneConnector); ok {
	//	fmt.Println("PhoneConnector Disconnect", usb.(PhoneConnector).Name())
	//}

	//swtich
	switch v := usb.(type) {
	case PhoneConnector:
		fmt.Println("PhoneConnector Disconnect", v.Name())
		//类型转换 typename(被转换对象)
		//fmt.Println("PhoneConnector Disconnect", PhoneConnector(v).Name())
	default:
		fmt.Println("Unknown Device")
	}

}

func main() {
	a := PhoneConnector{name: "phoneusb"}
	a.Connect()
	Disconnect(a)

	//未实现的接口的方法不能转换
	//b := TVConnector{"tv"}
	//b1 := Usb(b)
	//b1.Connect()

	var c interface{}
	fmt.Println(c == nil)

}
