package main

import "fmt"

const (
	a = 65
	b = iota
	c
	d
)
const e = iota

const (
	f = 63
	g = iota
	h = 64
	i = iota
)

//使用默认常量表达式和iota实现字节的单位
const (
	BYTE = 1 << (iota * 10)
	KB
	MB
	GB
)

func main() {
	fmt.Println(a, b, c, d)
	fmt.Println(e)
	fmt.Println(f, g, h, i)

	fmt.Println(BYTE)
	fmt.Println(KB)
	fmt.Println(MB)
	fmt.Println(GB)
}
