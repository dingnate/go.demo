package main

import "fmt"

func main() {
	for {
		for i := 0; i < 10; i++ {
			if i > 3 {
				// goto 是调整程序运行位置，goto的LABEL要放在goto语句之后，放在前面容易死循环
				goto LABEL1
			}
		}
	}
LABEL1:
	fmt.Println("OK")
}
