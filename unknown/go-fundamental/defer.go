package main

import "fmt"

func main() {
	fmt.Println("a")
	defer fmt.Println("b")
	defer fmt.Println("c")

	for i := 0; i < 3; i++ {
		defer fmt.Println(i)
		//这里是匿名函数内部的i是外部i引用，所以值是for循环执行后的值:3，这里用的是闭包的概念
		//defer func() { fmt.Println(i) }()
		//通过传参可以每次调用都更新打印的值
		//defer func(i1 int) { fmt.Println(i1) }(i)
	}
}
