package main

import "fmt"

type A7 struct {
	//小写成员的在包内、结构类型方法内可见
	name string
}

func main() {
	a := &A7{}
	a.name = "geo1"
	fmt.Println(a)
	a.Println()
	fmt.Println(a)
}

func (a *A7) Println() {
	a.name = "goe2"
}
