package main

import (
	"fmt"
)

type TZ int

func main() {
	a := A6{}
	a.Print()
	fmt.Println(a)
	b := B1{}
	b.Print()
	fmt.Println(b)
	i := TZ(1)
	//method value
	i.Println()
	//method description
	(*TZ).Println(&i)
	i1 := TZ(0)
	i1.Increase(100)
	fmt.Println(i1)
	i1.Increase(100)
	fmt.Println(i1)
}
func (a *TZ) Increase(num int) {
	//*a += TZ(100)
	*a += 100
}

//自定义类型的方法，自定义类型可以是基本类型的别名，此处是int的别名
func (a *TZ) Println() {
	fmt.Println(*a)
}

type A6 struct {
	Name string
}
type B1 struct {
	Name string
}

//结构体引用方法
func (a *A6) Print() {
	a.Name = "AA"
	fmt.Println(a)
}

//结构体值方法
func (b B1) Print() {
	b.Name = "BB"
	fmt.Println(b)
}
