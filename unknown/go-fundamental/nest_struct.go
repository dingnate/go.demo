package main

import "fmt"

type human struct {
	Sex int
}

type Teacher struct {
	human
	Name string
	Age  int
	Sex  int
}

func main() {
	a := &Teacher{Name: "joe1", Age: 19, human: human{0}}
	fmt.Println(*a)
	a.Sex = 1
	fmt.Println(*a)
	a.human.Sex = 1
	fmt.Println(*a)
}
