package main

import (
	"fmt"
)

func main() {
	var a [2]int
	var b [1]int
	fmt.Println(a)
	fmt.Println(b)
	//不同维度的数组类型不一样，不可以相互赋值
	//b=a

	var c [2]string
	fmt.Println(c)

	d := [2]int{1, 2}
	fmt.Println(d)

	e := [20]int{19: 1}
	fmt.Println(e)

	f := [...]int{1, 2, 3, 4}
	fmt.Println(f)

	g := [...]int{99: 1}
	//h:=&g
	var h *[100]int = &g
	fmt.Println(*h)

	i := [10]int{}
	i[1] = 1
	fmt.Println(i)
	// new返回的是对象的指针，数组指针操作元素的方式和数组对象一样
	j := new([10]int)
	j[1] = 1
	fmt.Println(j)

	//多维数组
	k := [2][3]int{{1, 1, 1}, {2, 2, 2}}
	fmt.Println(k)
	k[0][1] = 3
	k[1][1] = 3
	fmt.Println(k)

	l := [2][3]int{{1: 3}, {1: 3}}
	fmt.Println(l)

	//sort
	m := [...]int{5, 2, 6, 3, 9}
	fmt.Println(m)
	num := len(m)
	//冒泡排序
	for i := 0; i < num; i++ {
		for j := 1; j < num-i; j++ {
			if m[j-1] > m[j] {
				tmp := m[j]
				m[j] = m[j-1]
				m[j-1] = tmp
			}
		}
	}
	fmt.Println(m)

	//比较排序
	for i := 0; i < num-1; i++ {
		for j := i + 1; j < num; j++ {
			if m[j] < m[i] {
				tmp := m[j]
				m[j] = m[i]
				m[i] = tmp
			}
		}
	}
	fmt.Println(m)

	//数组比较
	n := [2]int{1, 2}
	o := [2]int{1, 2}
	p := [2]int{1, 1}
	fmt.Println(n == o)
	fmt.Println(n == p)
}
