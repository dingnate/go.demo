package main

import (
	"fmt"
)

type (
	byte uint8
	rune int16
	文本   string
)

const (
	PI_ = 3.14
)

func main() {
	var b 文本 = "字符类型的值"
	var c byte
	var r rune = 1111
	fmt.Println(b)
	fmt.Println(c)
	fmt.Println(r)
	fmt.Println(PI_)
}
