package main

import "fmt"

func main() {
	if 2 > 1 {
		fmt.Println("if true go here")
	}

	a := 1
	//if条件不加() 分号前面是初始化语句
	if a := 2; a > 1 {
		//if语句块中的a是内部变量，有同名变量时，外部变量a被隐藏
		fmt.Println(a)
	}
	//此处的变量a=1是外部变量，if语句块中的变量对外部不可见
	fmt.Println(a)
}
