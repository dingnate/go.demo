package main

import (
	"fmt"
	"strconv"
)

func main() {
	var a float32 = 100.1
	fmt.Println(a)

	//bool和float类型是不兼容的，不可以相互转换
	//b1 := bool(a)
	//fmt.Println(b1)

	//数字转字符串类型，默认是ascII码的转换
	var b int = 65
	var s string = string(b)
	fmt.Println(s)

	var s1 string = strconv.Itoa(b)
	fmt.Println(s1)
}
