package main

import (
	"fmt"
)

func main() {
	//for 1
	a := 1
	for {
		a++
		fmt.Println(a)
		if a > 3 {
			break
		}
	}
	fmt.Println("for 1 over")

	//for 2
	a = 1
	for a <= 3 {
		a++
		fmt.Println(a)
	}
	fmt.Println("for 2 over")

	//for 3
	a = 1
	for i := 0; i < 3; i++ {
		a++
		fmt.Println(a)
	}
	fmt.Println("for 3 over")

	//for 4
	s := "string"
	vlen := len(s)
	for i := 0; i < vlen; i++ {
		fmt.Println(string(s[i]))
	}
	fmt.Println("for 4 over")
}
