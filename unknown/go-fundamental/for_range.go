package main

import (
	"fmt"
	"sort"
)

func main() {
	sm := make([]map[int]string, 5)
	for i, _ := range sm {
		//赋值需要使用下标才能修改到sm中的值
		sm[i] = make(map[int]string)
		sm[i][1] = "OK"
		fmt.Println(sm[i])
	}
	fmt.Println(sm)

	m1 := map[int]string{1: "a", 2: "b", 3: "c", 4: "d", 5: "e"}
	s := make([]int, len(m1))
	i := 0
	for key, value := range m1 {
		// 无序的
		fmt.Println(key, value)
		s[i] = key
		i++
	}
	//对map的key进行排序
	sort.Ints(s)
	fmt.Println(s)

	//交换m1的key和value
	m2 := map[string]int{}
	for key, value := range m1 {
		m2[value] = key
	}
	fmt.Println(m2)
}
