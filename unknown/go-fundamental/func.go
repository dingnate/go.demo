package main

import (
	"fmt"
	"strconv"
)

func main() {
	A1(1, 2, 3, 4)
	a := 1
	A2(&a)
	fmt.Println(a)

	//匿名函数
	f1 := func() {
		fmt.Println("匿名函数")
	}
	f1()

	f2 := closure(1)
	i := f2(2)
	fmt.Println(strconv.Itoa(i))
}

func closure(x int) func(int) int {
	return func(y int) int {
		return x + y
	}
}

//指针类型参数
func A2(a *int) {
	*a = 2
}

// 不定长参数
func A1(a ...int) {
	//slice传递的是地址
	a[0] = 5
	fmt.Println(a)
}
