package main

import "fmt"

func main() {
	a := [10]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	var s1 []int = a[5:6]
	fmt.Println(s1)
	s1 = a[5:] //相当于 a[5:10]
	fmt.Println(s1)
	s1 = a[0:5]
	fmt.Println(s1)
	s2 := make([]int, 10, 10)
	s2 = a[2:7]
	fmt.Println(s2)

	//通过slice创建slice
	b := [...]byte{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k'}
	fmt.Println(b)
	s3 := b[2:5]
	fmt.Println(len(s3), cap(s3))
	s4 := s3[3:5]
	fmt.Println(string(s4))

	//长度超过cap 6时，slice指向重新分配地址
	c := make([]int, 3, 6)
	fmt.Printf("%p\n", c)
	c = append(c, 1, 2, 3)
	fmt.Printf("%v %p\n", c, c)
	c = append(c, 1, 2, 3)
	fmt.Printf("%v %p\n", c, c)

	//两个slice指向同一个数据，修改共同的元素，则两个slice都会收影响
	d := []int{1, 2, 3, 4, 5}
	s5 := d[2:5]
	s6 := d[1:3]
	fmt.Println(s5, s6)
	s5[0] = 9
	fmt.Println(s5, s6)

	//copy
	s7 := []int{1, 2, 3, 4, 5, 6}
	s8 := []int{7, 8, 9}
	fmt.Println(s7)
	fmt.Println(s8)
	//拷贝整个slice
	copy(s7, s8)
	fmt.Println(s7)
	//拷贝部分slice元素
	copy(s8[0:2], s7[4:6])
	fmt.Println(s8)

	//slice 完整拷贝
	s9 := s7[:]
	fmt.Println(s9)
}
