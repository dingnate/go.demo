package main

import "fmt"

type person struct {
	Name string
	Age  int
}

type person1 struct {
	Name    string
	Age     int
	Contact struct {
		Phone string
		City  string
	}
}
type person2 struct {
	Name string
	Age  int
}

func main() {
	a := &person{
		Name: "joe",
		Age:  19,
	}
	fmt.Println(a)
	A4(*a)
	fmt.Println(a)
	A5(a)
	fmt.Println(a)

	a1 := &person1{Name: "joe", Age: 19}
	a1.Contact.Phone = "133131313313"
	a1.Contact.City = "NanJing"
	fmt.Println(a1)
	//匿名赋值，按字段声明顺序
	a2 := &person2{"joe", 19}
	fmt.Println(a2)
	a3 := &person2{"joe", 19}
	//stuct  person2对象的比较
	fmt.Println(*a2 == *a3)
	a3.Age = 13
	fmt.Println(*a2 == *a3)

}

//值参数
func A4(per person) {
	per.Age = 13
	fmt.Println("Func A4", per)
}

//指针参数
func A5(per *person) {
	per.Age = 13
	fmt.Println("Func A5", per)
}
