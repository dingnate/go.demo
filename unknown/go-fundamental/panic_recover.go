package main

import "fmt"

func main() {
	A()
	B()
	C()

	fs := [4]func(){}
	for i := 0; i < 4; i++ {
		defer fmt.Println("defer i =", i)

		//下面用的到的是闭包  这里是匿名函数内部的i是外部i的引用，所以值是for循环执行后的值:4
		defer func() { fmt.Println("defer_closure i =", i) }()
		fs[i] = func() {
			fmt.Println("closure i =", i)
		}
	}
	for _, f := range fs {
		f()
	}
}

func A() {
	fmt.Println("Func A")

}

func B() {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("Recover in B")
		}
	}()
	panic("Panic in B")
}

func C() {
	fmt.Println("Func C")
}
