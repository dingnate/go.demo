package main

import (
	"fmt"
	"strconv"
)

func main() {
	//switch 1
	a := 1
	switch a {
	case 0:
		fmt.Println("a=0")
	case 1:
		fmt.Println("a=1")
	default:
		fmt.Println("None")
	}
	fmt.Println("switch 1 over")

	//switch 2
	a = 1
	switch {
	case a >= 0:
		fmt.Println("a>=0")
		// 使用fallthrough 可以继续执行下一个case
		fallthrough
	case a >= 1:
		fmt.Println("a>=1")
	default:
		fmt.Println("None")
	}

	fmt.Println("switch 2 over")

	//switch 3
	a = 2
	switch a := 1; {
	case a >= 0:
		fmt.Println("使用内部变量a", "a="+strconv.Itoa(a))
		fmt.Println("a>=0")
	case a >= 1:
		fmt.Println("a>=1")
	default:
		fmt.Println("None")
	}
	fmt.Println("使用外部变量a", "a="+strconv.Itoa(a))
	fmt.Println("switch 3 over")
}
