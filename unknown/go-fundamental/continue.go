package main

import "fmt"

func main() {
LABEL1:

	for i := 0; i < 10; i++ {
		for {
			// continue 是跳出label位置的本次循环，继续下一次循环
			continue LABEL1
			fmt.Println(i)
		}
	}

	fmt.Println("OK")
}
