package main

import "fmt"

func main() {
	m := map[int]string{}
	m[1] = "OK"
	fmt.Println(m)
	delete(m, 1)
	a := m[1]
	fmt.Println("a=" + a)

	// 初始化多维map时，默认只初始化最外层的map
	var m1 = make(map[int]map[int]string)
	a, ok := m1[2][1]
	if !ok {
		//声明初始化内部map
		m1[2] = make(map[int]string)
	}
	m1[2][1] = "GOOD"
	a, ok = m1[2][1]
	fmt.Println(a, ok)
}
