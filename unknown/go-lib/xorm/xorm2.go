package main

import (
	"fmt"
	"go.demo/unknown/go-lib/xorm/models"
	"log"
)

func main() {
	fmt.Println("Welcome to bank of xorm")

	count, err := models.GetAccountCount()
	if err != nil {
		log.Fatalf("Fail to get account count %v", err)
	}
	fmt.Println("Account count:", count)

	for i := count; i < 10; i++ {
		if err := models.NewAccount(fmt.Sprintf("jeo%d", i), float64(i)*100); err != nil {
			log.Fatalf("Fail to create account:%v\n", err)
		}
	}

	//Iterator迭代查询
	fmt.Println("query all records by iterator")
	models.IteratorPrintln()

	//Rows查询
	fmt.Println("Query all records by rows")
	models.RowPrintln()

	//Cols
	fmt.Println("Query all records by Cols")
	models.ColsNamePrintln()

	//Omit
	fmt.Println("Query all records by Omit")
	models.OmitNamePrintln()

	//limit
	fmt.Println("Query all records by limit")
	models.LimitPrintln()
}
