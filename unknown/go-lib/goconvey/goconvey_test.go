package goconvey

import (
	"github.com/smartystreets/assertions/should"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestAdd(t *testing.T) {
	Convey("将两数相加", t, func() { So(Add(1, 2), should.Equal, 3) })
}

func TestSubstract(t *testing.T) {
	Convey("将两数相减", t, func() { So(Substract(1, 2), should.Equal, -1) })
}

func TestMutiply(t *testing.T) {
	Convey("将两数相乘", t, func() { So(Mutiply(2, 3), should.Equal, 6) })
}
func TestDivision(t *testing.T) {
	Convey("将两数相除", t, func() {
		Convey("被除数为0", func() {
			_, err := Division(10, 0)
			So(err, ShouldNotBeNil)
		})
		Convey("被除数不为0", func() {
			i, err := Division(10, 2)
			So(err, ShouldBeNil)
			So(i, should.Equal, 5)
		})
	})
}
