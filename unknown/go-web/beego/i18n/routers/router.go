package routers

import (
	"github.com/astaxie/beego"
	"go.demo/unknown/go-web/beego/i18n/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})
}
