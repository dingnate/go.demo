package main

import (
	"github.com/astaxie/beego"
	"github.com/beego/i18n"
	_ "go.demo/unknown/go-web/beego/i18n/routers"
)

func main() {
	//国际化文件注册
	i18n.SetMessage("zh-CN", "conf/locale_zh-CN.ini")
	i18n.SetMessage("en-US", "conf/locale_en-US.ini")

	//注册模板函数
	beego.AddFuncMap("i18n", i18n.Tr)

	beego.Run()
}
