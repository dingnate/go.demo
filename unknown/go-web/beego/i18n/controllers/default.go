package controllers

import (
	"github.com/astaxie/beego"
	"github.com/beego/i18n"
)

type BaseController struct {
	beego.Controller
	i18n.Locale
}

type MainController struct {
	BaseController
}

func (this *MainController) Prepare() {
	lang := this.GetString("lang")
	if lang == "zh-CN" {
		this.Lang = lang
	} else {
		this.Lang = "en-US"
	}
	this.Data["Lang"] = this.Lang
}

func (this *MainController) Get() {
	this.Data["Website"] = "beego.me"
	this.Data["Email"] = "astaxie@gmail.com"
	this.TplName = "index.tpl"

	//http://localhost:8080
	//http://localhost:8080/?lang=zh-CN

	//Tr 函数
	//this.Data["Hi"] = this.Tr("hi")
	//this.Data["Bye"] = this.Tr("bye")

	//Tr 模板函数  {{i18n .Lang hi}}
	this.Data["Hi"] = "hi"
	this.Data["Bye"] = "bye"

	this.Data["About"] = "about"
}
