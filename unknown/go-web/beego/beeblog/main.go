package main

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/orm"
	"go.demo/unknown/go-web/beego/beeblog/models"
	_ "go.demo/unknown/go-web/beego/beeblog/routers"
)

func init() {
	models.RegisterDB()
}
func main() {
	orm.Debug = true

	orm.RunSyncdb("default", false, true)

	//附件作为静态文件
	//beego.SetStaticPath("/attachment", "attachment")
	beego.Run()
}
