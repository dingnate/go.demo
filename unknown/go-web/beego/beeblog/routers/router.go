package routers

import (
	"github.com/astaxie/beego"
	"go.demo/unknown/go-web/beego/beeblog/controllers"
)

func init() {
	beego.Router("/", &controllers.HomeController{})
	beego.Router("/login", &controllers.LoginController{})
	beego.Router("/category", &controllers.CategoryController{})
	beego.Router("/topic", &controllers.TopicController{})
	//path: /:controller/:method 	 http://localhost:8080/topic/add
	beego.AutoRouter(&controllers.TopicController{})
	beego.Router("/reply/add", &controllers.ReplyController{}, "post:Add")
	beego.Router("/reply/delete", &controllers.ReplyController{}, "get:Delete")
	//附件控制器
	beego.Router("/attachment/:id:int/:all", &controllers.AttachmentController{})
}
