package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"go.demo/unknown/go-web/beego/beeblog/models"
)

type HomeController struct {
	beego.Controller
}

func (this *HomeController) Get() {
	this.Data["IsHome"] = true
	this.TplName = "home.html"
	this.Data["IsLogin"] = checkAccount(this.Ctx)
	var err error
	if this.Data["Topics"], err = models.GetAllTopics(this.Input().Get("cate"), this.Input().Get("label"), true); err != nil {
		logs.Error(err)
	}
	if this.Data["Categories"], err = models.GetAllCategories(); err != nil {
		logs.Error(err)
	}
}
