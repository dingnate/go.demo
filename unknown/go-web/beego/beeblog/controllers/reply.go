package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"go.demo/unknown/go-web/beego/beeblog/models"
)

type ReplyController struct {
	beego.Controller
}

func (this *ReplyController) Delete() {
	rid := this.Input().Get("id")
	tid := this.Input().Get("tid")
	err := models.DeleteReply(rid, tid)
	if err != nil {
		logs.Error(err)
	}
	this.Redirect("/topic/view/"+tid, 302)
}
func (this *ReplyController) Add() {
	tid := this.Input().Get("tid")
	err := models.AddReply(tid, this.Input().Get("nickname"), this.Input().Get("content"))
	if err != nil {
		logs.Error(err)
	}
	this.Redirect("/topic/view/"+tid, 302)
}
