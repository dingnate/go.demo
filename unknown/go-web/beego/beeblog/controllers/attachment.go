package controllers

import (
	"github.com/astaxie/beego"
	"io"
	"net/url"
	"os"
)

type AttachmentController struct {
	beego.Controller
}

func (this *AttachmentController) Get() {
	if filePath, err := url.PathUnescape(this.Ctx.Request.RequestURI[1:]); err != nil {
		this.Ctx.WriteString(err.Error())
	} else {
		if f, err := os.Open(filePath); err != nil {
			this.Ctx.WriteString(err.Error())
		} else {
			defer f.Close()
			io.Copy(this.Ctx.ResponseWriter, f)
		}
	}
}
