package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"go.demo/unknown/go-web/beego/beeblog/models"
)

type CategoryController struct {
	beego.Controller
}

func (this *CategoryController) Get() {
	op := this.Input().Get("op")
	switch op {
	case "add":
		name := this.Input().Get("name")
		if len(name) == 0 {
			break
		}
		if err := models.AddCategory(name); err != nil {
			logs.Error(err)
		}
		this.Redirect("/category", 301)
		return
	case "del":
		id := this.Input().Get("id")
		if len(id) == 0 {
			break
		}
		if err := models.DelCategory(id); err != nil {
			logs.Error(err)
		}
		this.Redirect("/category", 301)
		return
	}
	this.Data["IsLogin"] = checkAccount(this.Ctx)
	this.TplName = "category.html"
	this.Data["IsCategory"] = true
	var err error
	if this.Data["Categories"], err = models.GetAllCategories(); err != nil {
		logs.Error(err)
	}
}
func (this *CategoryController) Post() {
	uname := this.Input().Get("uname")
	pwd := this.Input().Get("pwd")
	autoLogin := this.Input().Get("autoLogin") == "on"

	if beego.AppConfig.String("uname") == uname && beego.AppConfig.String("pwd") == pwd {
		maxAge := 0
		if autoLogin {
			maxAge = 1<<31 - 1
		}
		this.Ctx.SetCookie("uname", uname, maxAge, "/")
		this.Ctx.SetCookie("pwd", pwd, maxAge, "/")
	}
	this.Redirect("/", 301)
	return
}
