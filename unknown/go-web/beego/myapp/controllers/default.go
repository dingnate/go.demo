package controllers

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"strconv"
)

type MainController struct {
	beego.Controller
}

func (c *MainController) Get() {
	tplExample(c)
	//printAppConf(c)
	//defaultGet(c)
}
func tplExample(c *MainController) {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplName = "index.tpl"
	//true false var
	c.Data["TrueCond"] = true
	c.Data["FalseCond"] = false
	//struct var
	type u struct {
		Name string
		Age  int
		Sex  string
	}
	user := &u{
		Name: "jeo",
		Age:  20,
		Sex:  "Male",
	}
	c.Data["User"] = user
	//range
	Nums := [10]int{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}
	c.Data["Nums"] = Nums
	//tpl var
	c.Data["TplVar"] = "hey guys"
	//tpl func
	c.Data["html"] = "<div>hello beego</div>"
}

func printAppConf(c *MainController) {
	//通过key获取配置
	c.Ctx.WriteString("appname:" + beego.AppConfig.String("appname") +
		"\nhttpport:" + beego.AppConfig.String("httpport") +
		"\nrunmode:" + beego.AppConfig.String("runmode"))

	//变量方式获取配置
	c.Ctx.WriteString("\n\nappname:" + beego.BConfig.AppName +
		"\nhttpport:" + strconv.Itoa(beego.BConfig.Listen.HTTPPort) +
		"\nrunmode:" + beego.BConfig.RunMode)

	//日志信息 info级别的日志不会打印trace日志
	logs.SetLevel(logs.LevelInformational)
	logs.Trace("trace test1")
	logs.Info("info test1")
	//过期日志api
	//beego.SetLevel(beego.LevelInformational)
	//beego.Trace("trace test1")
	//beego.Info("info test1")

}

func defaultGet(c *MainController) {
	c.Data["Website"] = "beego.me"
	c.Data["Email"] = "astaxie@gmail.com"
	c.TplName = "index.tpl"
}
