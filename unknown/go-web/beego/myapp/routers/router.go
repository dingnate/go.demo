package routers

import (
	"github.com/astaxie/beego"
	"go.demo/unknown/go-web/beego/myapp/controllers"
)

func init() {
	beego.Router("/", &controllers.MainController{})
}
