package beego

import "github.com/astaxie/beego"

type HomeRouter struct {
	beego.Controller
}

func (this *HomeRouter) Get() {
	this.Ctx.WriteString("hello world")

}
func main() {
	beego.Router("/", &HomeRouter{})
	beego.Run()
}
