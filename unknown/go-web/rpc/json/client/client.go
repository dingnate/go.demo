package main

import (
	"fmt"
	"log"
	"net/rpc/jsonrpc"
	"os"
)

//go run client.go localhost:1234
func main() {
	if len(os.Args) != 2 {
		fmt.Println("Usage:", os.Args[0], "server")
		return
	}
	serverAddr := os.Args[1]

	client, err := jsonrpc.Dial("tcp", serverAddr)
	if err != nil {
		log.Fatal("dialing:", err)
	}

	args := Args{17, 8}
	var reply int
	err = client.Call("Math.Multiply", args, &reply)
	if err != nil {
		log.Fatal("Math error:", err)
	}
	fmt.Printf("Math: %d*%d=%d\n", args.A, args.B, reply)

	var quo Quotient

	err = client.Call("Math.Divide", args, &quo)
	if err != nil {
		log.Fatal("Math error:", err)
	}
	fmt.Printf("Math: %d/%d=%d remainder %d\n", args.A, args.B, quo.Quo, quo.Rem)
}

//实际使用时，客户端和服务段不在一起，需要把rpc调用时用到的类型引入
type Quotient struct {
	Quo, Rem int
}
type Args struct {
	A, B int
}
