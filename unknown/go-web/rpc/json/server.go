package main

import (
	"github.com/pkg/errors"
	"log"
	"net"
	"net/rpc"
	"net/rpc/jsonrpc"
	"os"
)

func main() {
	rpc.Register(new(Math))

	addr, err := net.ResolveTCPAddr("tcp", ":1234")
	if err != nil {
		log.Fatal(err.Error())
		os.Exit(2)
	}

	listener, err := net.ListenTCP("tcp", addr)
	if err != nil {
		log.Fatal(err.Error())
		os.Exit(2)
	}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Fatal("conn error:", err.Error())
			continue
		}
		jsonrpc.ServeConn(conn)
	}
}

type Quotient struct {
	Quo, Rem int
}
type Args struct {
	A, B int
}

func (m *Math) Divide(args *Args, quo *Quotient) error {
	if args.B == 0 {
		return errors.New("divide by zero")
	}
	quo.Quo = args.A / args.B
	quo.Rem = args.A % args.B
	return nil
}

type Math int

func (m *Math) Multiply(args *Args, reply *int) error {
	*reply = args.A * args.B
	return nil
}
