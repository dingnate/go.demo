package main

import (
	"fmt"
	"github.com/pkg/errors"
	"net/http"
	"net/rpc"
)

func main() {
	rpc.Register(new(Math))
	rpc.HandleHTTP()

	if err := http.ListenAndServe(":1234", nil); err != nil {
		fmt.Println(err)
	}
}

type Quotient struct {
	Quo, Rem int
}
type Args struct {
	A, B int
}

func (m *Math) Divide(args *Args, quo *Quotient) error {
	if args.B == 0 {
		return errors.New("divide by zero")
	}
	quo.Quo = args.A / args.B
	quo.Rem = args.A % args.B
	return nil
}

type Math int

func (m *Math) Multiply(args *Args, reply *int) error {
	*reply = args.A * args.B
	return nil
}
