/*
break in switch or select
break in case statement jump out of switch or select statement
*/
package main

import (
	"fmt"
)

func main() {
	fmt.Println("------------------break in for swich case--------------------")
	for i := 0; i < 2; i++ {
		fmt.Println("for", i)
		switch {
		case true:
			fmt.Println("switch break", i)
			break
		}
	}
	fmt.Println("------------------break in for select case--------------------")
	a := make(chan string)
	go func() {
		a <- "hello1"
		a <- "hello2"
	}()
	for i := 0; i < 2; i++ {
		fmt.Println("for", i)
		select {
		case v := <-a:
			fmt.Println("select break", i, v)
			break
		}
	}

}
