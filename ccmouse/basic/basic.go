package main

import (
	"fmt"
	"math"
	"math/cmplx"
)

func main() {
	//fmt.Println("hello world")
	//variableZeroValue()
	//variableInitialValue()
	//variableShorter()
	//fmt.Println(aa, ss, bb)
	//euler()
	//triangle()
	//constant()
	enums()
}

func enums() {
	//普通枚举类型
	const (
		cpp1    = 0
		java1   = 1
		python1 = 2
		golang1 = 3
	)
	fmt.Println(cpp1, java1, python1, golang1)
	//iota是golang语言的常量计数器,只能在常量的表达式中使用。iota在const关键字出现时将被重置为0
	//递增枚举类型
	const (
		cpp2 = iota
		java2
		python2
		golang2
	)

	//通过ｉota常量表达式声明字节单位
	const (
		b = 1 << (10 * iota)
		kb
		mb
		tb
		pb
	)
	fmt.Println(b, kb, mb, tb, pb)
}

func constant() {
	const filename = "abc.txt"
	const (
		a, b = 3, 4
	)
	var c int
	c = int(math.Sqrt(a*a + b*b))
	fmt.Println(filename, c)
}

func triangle() {
	var a, b int = 3, 4
	var c int

	//强制类型转换int->float64->int
	c = int(math.Sqrt(float64(a*a + b*b)))
	fmt.Println(c)
}

func euler() {
	c := 3 + 4i
	fmt.Println(cmplx.Abs(c))

	// e^(i*pi)+1=0  欧拉公式
	fmt.Println(cmplx.Pow(math.E, 1i*math.Pi) + 1)
	fmt.Println(cmplx.Exp(1i*math.Pi) + 1)

	//只保留小数点后3位
	fmt.Printf("%.3f\n", cmplx.Exp(1i*math.Pi)+1)
}

//var aa = 3
//var ss = "kkk"

// define variables in var()
var (
	aa = 3
	ss = "kkk"
	bb = true
)

// can`t do like this not in func statement
//bb := true

func variableShorter() {
	a, b, c, s := 3, 4, true, "def"
	b = 5
	fmt.Println(a, b, c, s)
}

func variableInitialValue() {
	var a, b int = 3, 4
	var c, d = true, "def"
	var s string = "abc"
	fmt.Println(a, b, s, c, d)
}

func variableZeroValue() {
	var a int
	var s string
	//%q表示要带引号
	fmt.Printf("%d %q\n", a, s)
}
