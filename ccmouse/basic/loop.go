package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func main() {
	/*	fmt.Println(
		convertToBin(5),  //101
		convertToBin(13), //1101
		convertToBin(72387885),
		convertToBin(0),
	)*/

	//printFile("basic/abc.txt")

	//dead loop
	//forever()

	s := `abc"d"
kkkk
123
sp`

	printFileContents(strings.NewReader(s))
}

func forever() {
	for {
		fmt.Println("abc")
	}
}

func printFile(filename string) {
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	printFileContents(file)
}

func printFileContents(reader io.Reader) {
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
}

func convertToBin(n int) string {
	if n == 0 {
		return "0"
	}
	result := ""
	for ; n > 0; n /= 2 {
		lsb := n % 2
		result = strconv.Itoa(lsb) + result
	}
	return result
}
