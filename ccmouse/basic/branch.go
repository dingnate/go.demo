package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	//testIf()
	testSwitch()
}

func testSwitch() {
	fmt.Println(
		grade(0),
		grade(59),
		grade(60),
		grade(82),
		grade(100),
	)
	fmt.Println(grade(101))
}
func grade(score int) string {
	g := ""
	switch {
	case score > 100 || score < 0:
		panic(fmt.Sprintf("Wrong score:%d", score))
	case score < 60:
		g = "F"
	case score < 80:
		g = "C"
	case score < 90:
		g = "B"
	case score <= 100:
		g = "A"
	}
	return g
}

func testIf() {
	const filename = "abc.txt"
	//contents, err := ioutil.ReadFile(filename)
	//if err != nil {
	if contents, err := ioutil.ReadFile(filename); err != nil {
		fmt.Println(err)
	} else {
		fmt.Printf("%s\n", contents)
	}
}
