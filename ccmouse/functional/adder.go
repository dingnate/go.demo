package main

import (
	"fmt"
)

func adder() func(int) int {
	sum := 0
	return func(v int) int {
		sum += v
		return sum
	}
}

type iAdder func(int) (int, iAdder)

func adder2(base int) iAdder {
	return func(v int) (int, iAdder) {
		return base + v, adder2(base + v)
	}
}

func main() {
	fmt.Println("----normal closure ")
	a := adder()
	for i := 0; i < 10; i++ {
		fmt.Printf("0+1+...+%d=%d\n", i, a(i))
	}
	fmt.Println("----orthodox closure ")
	a1 := adder2(0)
	s := 0
	for i := 0; i < 10; i++ {
		s, a1 = a1(i)
		fmt.Printf("0+1+...+%d=%d\n", i, s)
	}
}
