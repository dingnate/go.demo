package main

import "fmt"

type queueAny []interface{}

func (q *queueAny) isEmpty() bool {
	return len(*q) == 0
}

func (q *queueAny) pop() interface{} {
	head := (*q)[0]
	*q = (*q)[1:]
	return head
}

//限定参数类型为int
func (q *queueAny) pushInt(v int) {
	*q = append(*q, v)
}

func (q *queueAny) push(v interface{}) {
	*q = append(*q, v)
}

func main() {
	var q queueAny
	q.push(1)
	q.push("2")
	q.push(3)
	fmt.Println("q=", q)

	fmt.Println(q.pop())
	fmt.Println(q.pop())
	fmt.Println(q.isEmpty())
	fmt.Println(q.pop())
	fmt.Println(q.isEmpty())

}
