package main

import "fmt"

type queueInt []int

func (q *queueInt) isEmpty() bool {
	return len(*q) == 0
}

func (q *queueInt) pop() int {
	head := (*q)[0]
	*q = (*q)[1:]
	return head
}
func (q *queueInt) push(v int) {
	*q = append(*q, v)
}

func main() {
	var q queueInt
	q.push(1)
	q.push(2)
	q.push(3)
	fmt.Println("q=", q)

	fmt.Println(q.pop())
	fmt.Println(q.pop())
	fmt.Println(q.isEmpty())
	fmt.Println(q.pop())
	fmt.Println(q.isEmpty())

}
