package main

import (
	"fmt"
	"go.demo/ccmouse/tree"
)

type myNode struct {
	node *tree.Node
}

func (n *myNode) postOrder() {
	if n == nil || n.node == nil {
		return
	}

	left := myNode{n.node.Left}
	left.postOrder()
	n.node.Print()
	right := myNode{n.node.Right}
	right.postOrder()
}

func main() {
	var root tree.Node

	root = tree.Node{Value: 3}
	root.Left = &tree.Node{}
	root.Right = &tree.Node{5, nil, nil}
	root.Right.Left = new(tree.Node)
	root.Left.Right = tree.CreateNode(2)
	root.Right.Left.SetValue(4)
	fmt.Println("-----traverse")
	root.Traverse()
	fmt.Println("-----after wrapper with myNode")
	mynode := myNode{&root}
	mynode.postOrder()
}
