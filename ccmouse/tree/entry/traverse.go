package main

import (
	"fmt"
	"go.demo/ccmouse/tree"
)

func main() {
	var root tree.Node

	root = tree.Node{Value: 3}
	root.Left = &tree.Node{}
	root.Right = &tree.Node{5, nil, nil}
	root.Right.Left = new(tree.Node)
	root.Left.Right = tree.CreateNode(2)
	root.Print()
	//nodes := []treeNode{
	//	{value: 3},
	//	{},
	//	{6, nil, &root},
	//}
	//fmt.Println(nodes)

	fmt.Println("-----traverse")
	root.Traverse()

	fmt.Println("-----after set value 100")
	root.SetValue(4)
	root.Traverse()

	root.TraverseFunc(func(node *tree.Node) {
		fmt.Println("TraverseFunc:", node.Value)
	})
	nodeCount := 0
	root.TraverseFunc(func(node *tree.Node) {
		nodeCount += 1
	})
	fmt.Println("nodeCount:", nodeCount)

}
