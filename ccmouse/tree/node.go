package tree

import "fmt"

type Node struct {
	Value       int
	Left, Right *Node
}

//工厂函数
func CreateNode(value int) *Node {
	//go 语言中局部变量也可以返回给外部使用,但在C++不可以
	return &Node{Value: value}
}

func (node Node) Print() {
	fmt.Println(node.Value)
}

func (node *Node) SetValue(value int) {
	if node == nil {
		fmt.Println("set value to nil node")
		return
	}
	node.Value = value
}

//函数作为参数
func (node *Node) TraverseFunc(f func(node *Node)) {
	if node == nil {
		return
	}

	node.Left.TraverseFunc(f)
	f(node)
	node.Right.TraverseFunc(f)
}
func (node *Node) Traverse() {
	if node == nil {
		return
	}

	node.Left.Traverse()
	fmt.Println(node.Value)
	node.Right.Traverse()
}
