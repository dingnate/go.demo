package concurrent

import (
	"container/list"
	"sync"
)

// author dingnate
//SyncQueue safe queue
type SyncQueue struct {
	l list.List
	m sync.RWMutex
}

func (this *SyncQueue) Len() int {
	this.m.RLock()
	defer this.m.RUnlock()
	return this.l.Len()
}

// Front returns the first element of list l or nil if the list is empty.
func (this *SyncQueue) Front() *list.Element {
	this.m.RLock()
	defer this.m.RUnlock()
	return this.l.Front()
}

// Remove removes e from l if e is an element of list l.
// It returns the element value e.Value.
// The element must not be nil.
func (this *SyncQueue) Remove(e *list.Element) interface{} {
	this.m.Lock()
	defer this.m.Unlock()
	return this.l.Remove(e)
}

// PushBack inserts a new element e with value v at the back of list l and returns e.
func (this *SyncQueue) PushBack(v interface{}) *list.Element {
	this.m.Lock()
	defer this.m.Unlock()
	return this.l.PushBack(v)
}
