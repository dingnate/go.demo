package concurrent

import (
	"container/list"
)

// author dingnate
//SyncDueue safe queue
type SyncDueue struct {
	SyncQueue
}

// Back returns the last element of list l or nil if the list is empty.
func (this *SyncDueue) Back() *list.Element {
	this.m.RLock()
	defer this.m.RUnlock()
	return this.l.Back()
}

// PushFront inserts a new element e with value v at the front of list l and returns e.
func (this *SyncDueue) PushFront(v interface{}) *list.Element {
	this.m.Lock()
	defer this.m.Unlock()
	return this.l.PushFront(v)
}
