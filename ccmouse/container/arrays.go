package main

import "fmt"

//go 语言的参数传递只有一种就是值传递
func main() {
	var arr1 [5]int
	arr2 := [3]int{1, 3, 5}
	//[...]int 中的...不可以省略,因为省略...后声明的类型是slice而不再是数组了
	arr3 := [...]int{2, 4, 6, 8, 10}
	var grid [4][5]int
	fmt.Println(arr1, arr2, arr3)
	fmt.Println(grid)

	printArray(arr1)
	//Cannot use 'arr2' (type [3]int) as type [5]int
	//printArray(arr2)
	printArray(arr3)
	//go中啊数组作为函数参数是值传递
	fmt.Println(arr1, arr3)
}

func printArray(arr [5]int) {
	arr[0] = 100
	for _, v := range arr {
		fmt.Println(v)
	}
}
