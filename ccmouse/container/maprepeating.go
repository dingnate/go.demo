package main

import "fmt"

func main() {
	fmt.Println(lengthOfNonRepeatingSubStr("abcabcbb"))
	fmt.Println(lengthOfNonRepeatingSubStr("bbb"))
	fmt.Println(lengthOfNonRepeatingSubStr("pwwkew"))
	fmt.Println(lengthOfNonRepeatingSubStr(""))
	fmt.Println(lengthOfNonRepeatingSubStr("b"))
	fmt.Println(lengthOfNonRepeatingSubStr("abcdef"))
	fmt.Println(lengthOfNonRepeatingSubStr("一二三二一"))
	fmt.Println(lengthOfNonRepeatingSubStr("黑化肥挥发发灰会花飞灰化肥发黑会飞花"))
}
func lengthOfNonRepeatingSubStr(s string) int {
	lastOccurred := make(map[rune]int)
	//lastOccurred := make(map[byte]int)
	start, maxLength := 0, 0
	for i, v := range []rune(s) {
		//for i, v := range []byte(s) {
		if lastI, ok := lastOccurred[v]; ok && lastI >= start {
			start = lastI + 1
		}

		if tmpLen := i - start + 1; tmpLen > maxLength {
			maxLength = tmpLen
		}
		lastOccurred[v] = i
	}
	return maxLength
}
