package main

import "fmt"

//map使用哈希表,必须可以比较相等
//除了slice,map,function的内建类型都可以作为key
//Struct类ixng不包含上述字段,也可以作为key
func main() {
	m := map[string]string{
		"name":    "ccmouse",
		"course":  "golang",
		"site":    "imooc",
		"quality": "notbad",
	}

	m2 := make(map[string]string) //m2 == empty map
	var m3 map[string]string      //m3 ==nil
	fmt.Println("m3==nil is ", m3 == nil)
	// but can`t do this,otherwise will occur a panic
	//var m4 map[string]string = nil
	//m4["a"] = "v"

	fmt.Println(m, m2, m3)
	fmt.Println("------Traversing map")
	for k, v := range m {
		fmt.Println(k, v)
	}
	fmt.Println("-------print map key")
	for k := range m {
		fmt.Println(k)
	}
	fmt.Println("------print map value")
	for _, v := range m {
		fmt.Println(v)
	}
	fmt.Println("------get map value")
	courseName := m["course"]
	fmt.Println(courseName)
	causeName := m["cause"]
	fmt.Println(causeName)

	fmt.Println("------map contains key")
	// 判断map是否包含key
	ok := false
	if courseName, ok = m["course"]; ok {
		fmt.Println(courseName)
	} else {
		fmt.Println("key does not exist")
	}
	if causeName, ok = m["cause"]; ok {
		fmt.Println(courseName)
	} else {
		fmt.Println("key does not exist")
	}
	fmt.Println("------delete element from map")
	s, ok := m["name"]
	fmt.Println(s, ok)
	delete(m, "name")
	s, ok = m["name"]
	fmt.Println(s, ok)
}
