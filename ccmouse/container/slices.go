package main

import "fmt"

//re slice可以向后扩展,不可以向前扩展
//go 语言的变量是有零值(Zore value)的,虽然为nil但是程序还是可以运行
//append扩展slice时,如果发现len要超过cap则按cap*2扩充底层数组
//s8[4:]...中的...符号把slice类型转换为不定长参数类型
func main() {
	arr := [...]int{0, 1, 2, 3, 4, 5, 6, 7}
	fmt.Println("arr[2:6]=", arr[2:6])
	fmt.Println("arr[:6]=", arr[:6])
	s1 := arr[2:]
	fmt.Println("s1=", s1)
	s2 := arr[:]
	fmt.Println("s2=", s2)
	fmt.Println("updateSlice(s1)")
	updateSlice(s1)
	fmt.Println("s1=", s1)
	fmt.Println("arr=", arr)

	s1 = arr[2:6]
	//re slice
	s2 = s1[3:5]
	fmt.Printf("s1=%v len=%d cap=%d\n", s1, len(s1), cap(s1))
	fmt.Printf("s2=%v len=%d cap=%d\n", s2, len(s2), cap(s2))

	s3 := append(s2, 10)
	//append时超过cap值则底层重新分配大
	s4 := append(s3, 11)
	s5 := append(s4, 10)
	fmt.Println("s3,s4,s5=", s3, s4, s5)
	fmt.Printf(`s2指针=%p
s3指针=%p
s4指针=%p
s5指针=%p
`, s2, s3, s4, s5)
	fmt.Println(arr)

	//go 语言的变量是有零值(Zore value)的,虽然为nil但是程序还是可以运行
	var s6 []int
	fmt.Println("s6==nil is", s6 == nil)
	//append扩展slice时,如果发现len要超过cap则按cap*2扩充底层数组
	for i := 0; i < 10; i++ {
		printSlice(s6)
		s6 = append(s6, 1)
	}
	fmt.Println()
	s7 := []int{2, 4, 6, 8}
	printSlice(s7)
	s8 := make([]int, 16)
	printSlice(s8)
	s9 := make([]int, 10, 32)
	printSlice(s9)
	fmt.Println("Copy slice")
	copy(s8, s7)
	printSlice(s8)
	fmt.Println("Deleting element from  slice")
	//s8[4:]...中的...符号把slice类型转换为不定长参数类型
	s8 = append(s8[:3], s8[4:]...)
	printSlice(s8)
	fmt.Println("Popping from front")
	front := s8[0]
	fmt.Println("front", front)
	s8 = s8[1:]
	printSlice(s8)
	fmt.Println("Popping from back")
	tail := s8[len(s8)-1]
	fmt.Println("tail", tail)
	s8 = s8[:len(s8)-1]
	printSlice(s8)

}

func printSlice(s []int) {
	fmt.Printf(`指针=%p len=%d cap=%d value=%v
`, s, len(s), cap(s), s)
}

func updateSlice(s []int) {
	s[0] = 100
}
