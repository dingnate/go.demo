// if
package main

import (
	"fmt"
)

func main() {
	/*
		if
		条件表达式没有括号
		支持初始化表达式（可以是并行的）
		左大括号必须和条件语句或else在同一行
		支持单行模式
		初始化语句中的变量作用域为if语句块级别的，同时隐藏外部同名变量
	*/
	fmt.Println("-----------------if-------------------")
	a := 10
	if a, b := 1, 2; a+b > 0 {
		fmt.Println(a)
	}
	fmt.Println(a)
}
