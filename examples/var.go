/*
base type
var declare
set value

*/
// 当前程序的包名
package main

//包导入 包别名
//可以对const,var,type用()进行组操作
import (
	std "fmt"
	"math"
)

//首字母大写的变量，常量，函数是对外可见的（public），首字母小写则对外不可见（private）
//常量的定义
const PI = 3.14

//全局变量的声明与赋值,可以使用var()的方式简写，可以使用并行方式赋,但是不可以省略var
var (
	name        = "gopher"
	galobal_var = 100
)

type (
	//	文本 string //在utf-8中，可以定义中文的类型别名

	//一般类型的声明
	newType int

	//j结构的声明
	gopher struct{}

	//接口的声明
	golang interface{}
)

//由main函数作为程序入口点启动
func main() {
	//fmt.Println("Hello world!你好，世界！")
	std.Println("Hello world!你好，世界！")
	//小写为私有的，大写的可以被外部调用
	//	std.println("Hello world!你好，世界！"+std.commaSpaceString)

	//go基本类型
	std.Println(10 + newType(10))
	std.Println(bool(true))
	std.Println(int(255))  //根据os的32还是64
	std.Println(uint(255)) //根据os的32还是64
	std.Println(int8(-128))
	std.Println(uint8(255))
	std.Println(byte(255))
	std.Println(int16(-32768))
	std.Println(uint16(65535))
	std.Println(int32(-2))     //-2^32/2
	std.Println(rune(-2))      //-2^32/2  int32的别名
	std.Println(uint32(1))     //2^32-1
	std.Println(int64(-2))     //-2^64/2
	std.Println(uint64(1))     //2^64-1
	std.Println(float32(-2))   //精确到7位小数位
	std.Println(float64(1))    //精确到15位小数位
	std.Println(complex64(-2)) //8个字节
	std.Println(complex128(1)) //16个字节
	std.Println(uintptr(1))    //保存int指针类型的类型，根据os的32还是64
	std.Println(uintptr(1))    //保存int指针类型的类型，根据os的32还是64
	//类型零值（初始值）一般为0，bool为false,string为""
	var a0 int
	std.Println(a0)
	var a1 float32
	std.Println(a1)
	var a2 bool
	std.Println(a2)
	var a3 string
	std.Println(a3)
	var a4 []int
	std.Println(a4)
	var a5 [1]int
	std.Println(a5)
	var a6 [1]bool
	std.Println(a6)
	var a7 [1]byte
	std.Println(a7)

	//类型范围
	std.Println(math.MaxInt8)
	std.Println(math.MinInt8)

	////中文类型别名使用
	//	var a8 文本
	//	a8 = "中文字符"
	//	std.Println(a8)

	//单变量的声明与赋值
	var a9 int //变量的声明
	a9 = 123   //变量的赋值
	std.Println(a9)
	var a10 = 1 //省略了变量名由系统推断
	std.Println(a10)
	a11 := bool(false) //变量声明与赋值的最简写法，:只能给局部变量赋值,:之前不能再加类型了
	std.Println(a11)

	//多变量的声明与赋值
	//局部变量不可以使用var()的方式进行简写，:作为var的替代，用在局部变量声明的时候
	//	var a12, a13, a14 int = 1, 2, 3
	//	var a12, a13, a14 = 1, 2, 3
	a12, _, a13, a14 := 1, 2, 3, 4 //_作为忽略符号，忽略了第二个值
	std.Println(a12, a13, a14)
	a12, a13, _, a14 = 1, 2, 3, 4 //_作为忽略符号，忽略了第三个值
	std.Println(a12, a13, a14)

}
