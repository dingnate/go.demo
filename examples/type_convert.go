// type_convert
package main

import (
	std "fmt"
	"strconv"
)

func main() {
	a1 := 65
	a2 := strconv.Itoa(a1)
	std.Println(a2)
	std.Println(strconv.Atoi(a2))
	a3, _ := strconv.Atoi(a2)
	std.Println(a3)

	//变量的类型转换，转换只能发生在两种相互兼容的类型之间
	var a4 float32 = 1.0 //浮点类型
	std.Println(a4)
	a5 := int(a4) //int类型
	std.Println(a5)
}
