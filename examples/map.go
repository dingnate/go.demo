/*
map
类似其它语言的哈希表或者字典，以key-value形式存储数据
key必须是支持==或!=比较运算的类型，不可以是函数，map或slice
Map的查找比线性查找快很多，但比使用索引访问数据的类型慢100倍
map使用make创建，支持:=这种简写方式

make([keyType]valueType,cap),cap表示容量可以省略，超出容量时会自动扩容，但尽量提供一个合理但初始值
使用len获取元素个数

键值对不存在时自动添加，使用delete()删除某键值对
使用for range对map和slice进行迭代操作
*/
package main

import (
	"fmt"
)

func main() {
	fmt.Println("-----------------map---------------------")
	var a1 map[int]int
	fmt.Println(a1)
	var a2 = map[int]int{}
	fmt.Println(a2)
	a3 := map[int]int{1: 2, 2: 3}
	fmt.Println(a3)
	a4 := make(map[int]int, 2)
	fmt.Println(a4)
	a4[1] = 2 //向map中存入键值对
	a4[2] = 3
	fmt.Println(a4)
	delete(a4, 2) //删除键值对
	fmt.Println(a4)
	fmt.Println("-----------------多层map---------------------")
	var b map[int]map[int]string
	b = make(map[int]map[int]string)
	a, ok := b[2][1] //多返回值，存在键则返回true，否则返回false
	if !ok {
		b[2] = make(map[int]string)
	}
	b[2][1] = "1-value" //给内层map赋值当时候，需要像上面先初始化，否则会有运行时错误 panic: assignment to entry in nil map
	a, ok = b[2][1]     //多返回值，存在键则返回true，否则返回false
	fmt.Println(a, ok)
	fmt.Println("-----------------map键值交换---------------------")
	c := map[int]string{1: "a", 2: "b", 3: "c", 4: "d", 5: "e"}
	c1 := make(map[string]int, len(c))
	for k, v := range c {
		c1[v] = k
	}
	fmt.Println(c)
	fmt.Println(c1)
}
