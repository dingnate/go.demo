// for
package main

import (
	"fmt"
)

func main() {
	/*
		for
		go语言唯一的循环控制块

	*/
	fmt.Println("-----------------for1-------------------")
	a := 1
	for {
		if a > 3 {
			break
		}
		fmt.Println(a)
		a++
	}
	fmt.Println("over")

	fmt.Println("-----------------for2-------------------")
	a = 1
	for a <= 3 {
		fmt.Println(a)
		a++
	}
	fmt.Println("over")

	fmt.Println("-----------------for3-------------------")
	a = 0
	for a = 1; a <= 3; a++ {
		fmt.Println(a)

	}
	fmt.Println("over")
}
