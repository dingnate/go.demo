// sort
package main

import (
	"fmt"
	"sort"
)

func main() {
	a := [...]int{5, 2, 6, 3, 9}
	fmt.Println(a)

	fmt.Println("-------------------冒泡排序--------------------")
	lenght := len(a) //不可以使用len关键字声明变量，否则后面len()不可用
	var temp int
	count := 0
	for i := 0; i < lenght; i++ {
		for j := lenght - 1; j > i; j-- {
			if a[j] < a[j-1] {
				count++
				temp = a[j]
				a[j] = a[j-1]
				a[j-1] = temp
			}
		}
		/*
			for j := i + 1; j < lenght; j++ {
				if a[j] < a[i] {
					c++
					temp = a[j]
					a[j] = a[i]
					a[i] = temp
				}
			}
		*/
	}

	fmt.Println(a, count)
	fmt.Println("-------------------map key排序--------------------")
	b := map[int]string{1: "a", 2: "b", 3: "c", 4: "d", 5: "e"}
	fmt.Println(b)
	b1 := make([]int, len(b))
	b2 := 0
	for k, _ := range b {
		b1[b2] = k
		b2++
	}
	sort.Ints(b1)
	fmt.Println(b1)
}
