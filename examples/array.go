package main

import (
	"fmt"
)

func main() {
	/*
		array
		定义数组的格式：var <varname> [n] <type> ,n>=0
		数组的长度也是类型的一部分，因此具有不同长度的数组为不同类型
		注意区分指向数组的指针和指针数组
		数组在go中为值类型
		数组之间可以使用==或!=进行比较，但不可以使用<或>
		可以使用new来创建数组，此方法返回一个指向数组的指针
		go支持多维数组

	*/

	var a [2]int              //声明数组
	a[0] = 1                  //通过下标给数组元素赋值
	var b = [2]int{1}         //声明并初始化数组的第一个元素
	c := [1]string{"a"}       //用:声明并初始化数组
	d := b                    //用数组给数组变量赋值
	e := [3]int{2: 1}         //声明并初始化下标为2的元素值为1
	f := [...]int{1: 1, 3: 1} //使用...声明包含获取下标1值为1，下标3值为1的集合的最小数组
	fmt.Println(a, b, c, d, e, f)
	fmt.Println("f的长度为 ：", len(f)) //打印数组长度
	fmt.Println("a==b ：", a == b)
	fmt.Println("a!=b ：", a != b)
	a[1] = 1
	fmt.Println("a[1] = 1, a==b ：", a == b)

	fmt.Println("-----------------array pointer-------------------")
	var g *[2]int = &a
	fmt.Println(g)

	fmt.Println("----------------- pointer array-------------------")
	h, i := 1, 2
	j := [...]*int{&h, &i}
	//	j := [2]*int{&h, &i}
	fmt.Println(j)

	k := new([2]int) //用new声明数组，返回数组的指针
	fmt.Println(*k)
	k[1] = 1 //数组指针也可以通过下标给数组元素赋值
	fmt.Println(*k)

	l := [2][2]int{{11, 12}, {21, 22}} //多维数组
	fmt.Println(l)
	l = [2][2]int{{0: 1}, {1: 22}} //多维数组通过下标初始化
	fmt.Println(l)
	m := [...][2]int{{0: 11}, {1: 22}, {0: 31}} //通过第一维使用...声明包含集合的最小多维数组
	fmt.Println(m)
}
