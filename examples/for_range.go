/*
for_range
for i,v:=range slice{
	// i为index,v为slice中元素值
	slice[i]=v2
}

for k,v:=range map{
	//k为键，v为值
}
*/
package main

import (
	"fmt"
)

func main() {
	sm := make([]map[int]string, 5)
	for i, v := range sm {
		//		v = map[int]string{1: "ok"} //v是slice中元素值的拷贝，所以v的操作不会影响到slice本身

		//		sm[i] = make(map[int]string, 1)
		//		sm[i][1] = "ok"
		sm[i] = map[int]string{1: "ok"}

		fmt.Println(i, v, sm[i])
	}
	fmt.Println(sm)

}
