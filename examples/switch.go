// switch
package main

import (
	"fmt"
)

func main() {
	/*
		switch
		可以使用任何类型和表达式作为条件语句
		不需要break，一旦条件符合自动终止
		如希望继续执行下一个case，需要使用fallthrough语句
		支持一个初始化表达式（可以是并行方式），右侧需跟分号
		左侧大括号必须和条件语句在同一行
	*/
	fmt.Println("-----------------switch1-------------------")
	a := 0
	switch a {
	case 0:
		fmt.Println("a=0")
	case 1:
		fmt.Println("a=1")
	default:
		fmt.Println("None")
	}
	fmt.Println("-----------------switch2-------------------")
	a = 1
	switch {
	case a >= 0:
		fmt.Println("a>=0")
		fallthrough
	case a >= 1:
		fmt.Println("a>=1")
	default:
		fmt.Println("None")
	}

	fmt.Println("-----------------switch3-------------------")
	switch a = 1; {
	case a >= 0:
		fmt.Println("a>=0")
		fallthrough
	case a >= 1:
		fmt.Println("a>=1")
	default:
		fmt.Println("None")
	}
}
