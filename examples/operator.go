// operator
package main

import (
	"fmt"
)

func main() {
	//运算符优先级由高到低
	//一元运算符
	//优先级1
	fmt.Println(^127)   //对位取反
	fmt.Println(!false) //取非

	//二元运算符
	//优先级2
	fmt.Println(1 * 2)      //乘法
	fmt.Println(5 / 2)      //除法
	fmt.Println(5 % 2)      //取余
	fmt.Println(1 << 10)    //左移位
	fmt.Println(1024 >> 10) //右移位

	/*
		 6: 0110
		11: 1011
		---------
		&	0010=2
		|	1111=15
		^	1101=13
		&^  0100=4// 右数位值为1,则取0，右数位值为0，取左数位值
	*/
	fmt.Println(6 & 11)  //位与
	fmt.Println(6 &^ 11) //位与

	//优先级3
	fmt.Println(6 + 11) //加
	fmt.Println(11 - 6) //减
	fmt.Println(6 | 11) //位或
	fmt.Println(6 ^ 11) //位异或

	//优先级4
	fmt.Println(6 == 11)
	fmt.Println(6 != 11)
	fmt.Println(6 < 11)
	fmt.Println(6 <= 11)
	fmt.Println(6 >= 11)
	fmt.Println(6 > 11)

	//	优先级5

	//优先级6
	fmt.Println(true && 1 == 0)
	//优先级7
	fmt.Println(true || 1 == 0)

	a1 := 0
	//	a = 1
	if a1 > 0 && 10/a1 > 1 {
		fmt.Println("ok")
	} else {
		fmt.Println("fail")
	}

	//用常量iota获取存储单位的大小，单位Byte
	const (
		B uint64 = 1 << (iota * 10)
		KB
		MB
		GB
	)
	fmt.Println(B, KB, MB, GB)

	//	指针 操作对象成员用. 不可以使用-> 通过*p获取指针p所指的对象 通过&b获b的指针
	a2 := 1
	a2++ //++ --只能作为语句使用，不能作为=号右边的表达式
	var a3 *int = &a2
	fmt.Println(a3)
	fmt.Println(*a3)
	a4 := &a2
	fmt.Println(a4)
	a2--
	fmt.Println(*a4)

}
