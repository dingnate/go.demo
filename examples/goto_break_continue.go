// goto_break_continue
package main

import (
	"fmt"
)

func main() {

	/*
		跳转语句 goto，break，continue
		三个语法都可以配合标签使用
		标签名区分大小写，若不使用会造成编译错误
		break与continue配合标签可用于多层循环的跳出
		goto是调整执行位置，与其它2个语句配合标签的结果并不相同
	*/
	fmt.Println("-----------------break-------------------")
LABEL1:
	for {
		for i := 0; i < 10; i++ {
			if i > 3 {
				break LABEL1
			}
		}
	}
	fmt.Println("ok:break LABEL1")
	fmt.Println("-----------------continue-------------------")
LABEL2:
	for i := 0; i < 10; i++ {
		for {
			continue LABEL2
		}
	}

	fmt.Println("ok:continue LABEL2")

	fmt.Println("-----------------goto-------------------")

	for i := 0; i < 10; i++ {
		for {
			goto LABEL3 //goto是调整程序执行位置，标签一般放在后面
		}
	}
LABEL3:
	fmt.Println("ok:goto LABEL3")
}
