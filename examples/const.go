// const
package main

import (
	"fmt"
)

const a12 = '3'

const (
	// 同时定义多个常量
	//常量的初始化必须是常量,或者常量表达式，在本地常量声明时值表达式中不能包含组内常量
	a9, a10, a11 = "456", len(a9), a12 * 10
)

func main() {
	fmt.Println("Hello World!")
	//定义单个常量
	const a1 int = 1
	const a2 = 'a'
	const (
		a3 = "123"
		a4 = len(a3)
		a5 = a2 * 20
		a13
		a14, a15 = 1, 2
		a16, a17 ////组内常量没有初始化，则默认值为组内上行表达式的值，且当前行的常量要和上行常量的数量一致
	)
	fmt.Println(a13)
	fmt.Println(a16, a17)
	const a6, a7, a8 = 1, "2", '3'

	const (
		a18 = 'A'
		a19
		a20 = iota //iota是常量组中常量的index,iota初始值为0，常量组内每定义个常量自增1
		a21
	)
	fmt.Println(a18, a19, a20, a21)

}
